# std
import unittest

# 3rd party
import click
import click.testing

# local
from pipeline import pipeline


class PipelineTestCase(unittest.TestCase):
    def setUp(self):
        @click.group()
        def cli():
            click.echo('cli')

        @pipeline(cli, help='Test Pipeline')
        def pipe():
            click.echo('pipe:start')
            yield
            click.echo('pipe:end')

        @pipe.operation
        @click.option('--name', help='Name of the operation', default='op')
        def op(name):
            click.echo('pipe:{}'.format(name))

        self.cli = cli
        self.runner = click.testing.CliRunner()

    def test_pipeline_basic(self):
        expected = '\n'.join([
            'cli',
            'pipe:start',
            'pipe:op',
            'pipe:end',
            ''
        ])

        result = self.runner.invoke(self.cli, ['pipe', 'op'])

        self.assertEqual(result.exit_code, 0)
        self.assertEqual(expected, result.output)

    def test_pipeline_validate_order(self):
        expected = '\n'.join([
            'cli',
            'pipe:start',
            'pipe:op-1',
            'pipe:op-2',
            'pipe:end',
            ''
        ])

        result = self.runner.invoke(self.cli, ['pipe', 'op', '--name=op-1', 'op', '--name=op-2'])

        self.assertEqual(expected, result.output)
