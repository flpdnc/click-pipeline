#!/usr/bin/env python
from setuptools import setup
from setuptools.command.develop import develop

import os


class SetupDevelop(develop):
    """
    Command that configures the current virtualenv for development. This command will fail if it is invoked outside 
    of an active virtualenv. Invoked via::

    ```
    ./setup.py develop
    ```

    """
    def finalize_options(self):
        # Check to make sure we are in a virtual environment before we all this command to be run
        if not os.getenv('VIRTUAL_ENV'):
            print >>sys.stderr, 'ERROR: You are not in a virtual environment'
            sys.exit(1)

        develop.finalize_options(self)

    def run(self):
        """ Execute command pip for development requirements. """
        assert os.getenv('VIRTUAL_ENV'), 'You must be in a virtualenv'
        develop.run(self)
        self.spawn(
            ('pip', 'install', '--requirement', 'requirements/dev.txt')
        )


setup(
    author='Zero X Oneb',
    author_email='zeroxoneb@gmail.com',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Software Development',
    ],
    cmdclass={
        'develop': SetupDevelop
    },
    description='Utility to easily create click command pipelines',
    install_requires=[
        'click == 6.7',
    ],
    keywords=[
        'click',
        'pipeline',
    ],
    license='MIT',
    name='click-pipeline',
    packages=[
        'pipeline',
    ],
    url='https://gitlab.com/zeroxoneb/click-pipeline',
    version='2017.12.27',
)
