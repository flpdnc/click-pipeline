import click
import contextlib
import functools


####
# pipeline click decorator
####
def pipeline(group, name=None, **kwargs):
    """
    This decorator wraps a method that would normally be wrapped with a
    `click.group()` decorator and provides:
     - It handles the resultcallback() decorator such that the method beind
       wrapped ends up being the actual method being called.
     - It decorates the returned `click.Group` instance with an `operation`
       decorator so that additional methods can be attached to the group as a command.

    A complete (and simple) example::

    ```
    from pipeline import pipeline

    @click.group()
    def cmd():
      pass

    @pipeline(cmd, help='A pipeline command')
    def pipe():
      print('do stuff before pipe operations are invoked')
      yield
      print('do stuff after pipe operations are invoked')

    @pipe.operation
    def op():
      print('pipe operation doing stuff')
    ```
    """
    # This method processes the constructed pipeline executing the wrapped
    # 'pipeline' method as a context manager and then executing each supplied
    # 'operation' in the order they are passed on the CLI before returning
    # control back to the 'pipeline' method
    def processor(f):
        @functools.wraps(f)
        def execute(ops, *args, **kwargs):
            with f(*args, **kwargs):
                for op in ops:
                    for _ in op:
                        pass

        return execute

    # This is the decorator that will wrap the method that does the actual work
    def __pipeline(f):
        group_name = name or f.__name__
        pipeline_group = group.group(group_name, chain=True, **kwargs)(f)
        pipeline_group.resultcallback()(processor(contextlib.contextmanager(f)))

        def operation(op_f):
            op_name = op_f if isinstance(op_f, str) else op_f.__name__

            def decorator(_real_op_f):
                @functools.wraps(_real_op_f)
                def __operation(*args, **kwargs):
                    yield _real_op_f(*args, **kwargs)

                pipeline_group.add_command(click.command(op_name)(__operation))
                return __operation

            if isinstance(op_f, str):
                # we got an operation name instead of a function
                return decorator
            else:
                return decorator(op_f)

        # Attach the @operation decorator to the returned group
        pipeline_group.operation = operation
        return pipeline_group

    # Return the decorator that will do the actual work on the wrapped
    # method
    return __pipeline
