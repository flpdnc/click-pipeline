#!/usr/bin/env python

import attrdict
import click
import logging

log = logging.getLogger('pipeline')
logging.basicConfig(level=logging.DEBUG)

from pipeline import pipeline

####
# CLI entry point
####
@click.group()
@click.pass_context
def cli(ctx):
    logging.basicConfig(level=logging.DEBUG)
    ctx.obj.log = logging.getLogger('pipeline')


# @cli.group('main', help='Top level command')
# @click.pass_context
# def main(ctx):
 #   logging.basicConfig(level=logging.DEBUG)
 #   ctx.obj.log = logging.getLogger('pipeline')



@pipeline(cli, help='A pipeline to perform 1+ operations within')
@click.option('--arg',
              default='value',
              help='Arg to pass to the pipeline')
@click.pass_context
def pipe(ctx, arg=None):
    ctx.obj.log.debug('>> pipe(arg:{})'.format(arg))
    yield
    ctx.obj.log.debug('<< pipe(arg:{})'.format(arg))


######
# Pipeline operations
######
@pipe.operation('op')
@click.option('--name',
              default='op',
              help='Name of the option')
@click.pass_context
def op(ctx, name):
    ctx.obj.log.debug('op:doit(name: {})'.format(name))

# pipe.operation('noop')(op)


if __name__ == '__main__':
    cli(obj=attrdict.AttrDict())
