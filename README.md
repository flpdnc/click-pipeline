Click Pipeline
==============

[![pipeline status](https://gitlab.com/zeroxoneb/click-pipeline/badges/master/pipeline.svg)](https://gitlab.com/zeroxoneb/click-pipeline/commits/master)
[![coverage report](https://gitlab.com/zeroxoneb/click-pipeline/badges/master/coverage.svg)](https://gitlab.com/zeroxoneb/click-pipeline/commits/master)

A plugin for `Click` that makes the process of implementing 
[Multi Command Pipelines](http://click.pocoo.org/5/commands/#multi-command-pipelines) easier.
